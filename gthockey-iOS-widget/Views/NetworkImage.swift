//
//  NetworkImage.swift
//  gthockey-iOS-widgetExtension
//
//  Created by Caleb Rudnicki on 10/29/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import SwiftUI

struct NetworkImage: View {
    
    // MARK: Variables
    
    let url: URL?
    
    // MARK: Body
    
    var body: some View {
        Group {
            if let url = url, let imageData = try? Data(contentsOf: url),
               let uiImage = UIImage(data: imageData) {
                
                Image(uiImage: uiImage)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
            } else {
                Image("placeholder-image")
            }
        }
    }
}

struct NetworkImage_Previews: PreviewProvider {
    static var previews: some View {
        NetworkImage(url: URL(string: ""))
    }
}
