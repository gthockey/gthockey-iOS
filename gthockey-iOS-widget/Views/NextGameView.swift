//
//  NextGameView.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 10/24/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import SwiftUI
import WidgetKit

@available(iOS 14.0, *)
struct NextGameView: View {
    
    // MARK: Environment Variables
    
    @Environment(\.widgetFamily) var family

    // MARK: State Variables

    @State var game: Game
    @State var record: String
    
    // MARK: Body

    @ViewBuilder
    var body: some View {
        switch family {
        case .systemSmall:
            VStack(alignment: .center, spacing: .medium) {
                NextGameViewLogoComponent(game: game)
                VStack(alignment: .center) {
                    if game.shortResult == .NotReported {
                        Text("In Progress")
                            .bold()
                            .minimumScaleFactor(0.5)
                        Text("..awaiting result..")
                            .lineLimit(1)
                            .minimumScaleFactor(0.5)
                    } else {
                        Text(game.timestamp ??
                                "\(game.date! + " " + game.time!)".testDate,
                             style: .relative)
                            .bold()
                            .minimumScaleFactor(0.5)
                            .multilineTextAlignment(.center)
                        Text("until puckdrop")
                            .minimumScaleFactor(0.5)
                    }
                }
            }
            .padding()
        case .systemMedium:
            HStack(alignment: .center, spacing: .large) {
                VStack(alignment: .center, spacing: .medium) {
                    Text("Record")
                        .bold()
                        .font(.title)
                    Text(record)
                        .font(.title2)
                }
                .padding(.horizontal)
                VStack(alignment: .center, spacing: .none) {
                    Text("Next Game")
                        .bold()
                        .minimumScaleFactor(0.25)
                    NextGameViewLogoComponent(game: game)
                    VStack(alignment: .center) {
                        if game.shortResult == .NotReported {
                            Text("In Progress")
                                .bold()
                                .minimumScaleFactor(0.25)
                            Text("..awaiting result..")
                                .lineLimit(1)
                                .minimumScaleFactor(0.25)
                        } else {
                            Text(game.timestamp ??
                                    "\(game.date! + " " + game.time!)".testDate,
                                 style: .relative)
                                .bold()
                                .minimumScaleFactor(0.25)
                                .multilineTextAlignment(.center)
                            Text("until puckdrop")
                                .minimumScaleFactor(0.25)
                        }
                    }
                }
                .padding(.horizontal)
            }
            .padding()
        default: HStack{}
        }
    }
    
    func getSeasonString(from season: Season) -> String {
        return season.name.components(separatedBy: " ")[0]
    }
}

#if DEBUG
struct NextGameView_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 14.0, *) {
            NextGameView(game: GTHTestData().gameUpcomingTournament, record: "2-1-0-0")
                .previewLayout(.fixed(width: 200, height: 200))
        }
    }
}
#endif
