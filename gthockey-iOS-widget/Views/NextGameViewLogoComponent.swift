//
//  NextGameViewLogoComponent.swift
//  gthockey-iOS-widgetExtension
//
//  Created by Caleb Rudnicki on 10/29/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import SwiftUI

struct NextGameViewLogoComponent: View {
    
    // MARK: Variables
    
    let game: Game
    
    // MARK: Body
    
    var body: some View {
        HStack {
            if game.venue == .Home {
                NetworkImage(url: game.opponent.logoImageURL)
                    .scaledToFit()
                    .frame(maxWidth: 100, maxHeight: 100)
                Text("vs.")
                Image("BuzzOnlyLogo")
                    .resizable()
                    .scaledToFit()
                    .frame(maxWidth: 100, maxHeight: 100)
            } else {
                Image("BuzzOnlyLogo")
                    .resizable()
                    .scaledToFit()
                    .frame(maxWidth: 100, maxHeight: 100)
                Text("vs.")
                NetworkImage(url: game.opponent.logoImageURL)
                    .scaledToFit()
                    .frame(maxWidth: 100, maxHeight: 100)
            }
        }
    }
}

#if DEBUG
struct NextGameViewLogoComponent_Previews: PreviewProvider {
    static var previews: some View {
        NextGameViewLogoComponent(game: GTHTestData().gameUpcomingHome)
    }
}
#endif
