//
//  gthockey_iOS_widget.swift
//  gthockey-iOS-widget
//
//  Created by Caleb Rudnicki on 10/26/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import WidgetKit
import SwiftUI

struct Provider: TimelineProvider {
    
    let testModel = GTHTestData().gameUpcomingTournament
    
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date(), widgetData: testModel, widgetRecord: "2-1-0-0")
    }

    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date(), widgetData: testModel, widgetRecord: "2-1-0-0")
        completion(entry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []
        
        getData(completion: { games in
            let record = getRecord(with: games)
            let currentDate = Date()
            let entryDate = Calendar.current.date(byAdding: .second, value: 0, to: currentDate)!
            let entry = SimpleEntry(date: entryDate,
                                    widgetData: games.first(where: {$0.shortResult == .Unknown || $0.shortResult == .NotReported}) ?? games[0],
                                    widgetRecord: record)
            entries.append(entry)
            let timeline = Timeline(entries: entries, policy: .atEnd)
            completion(timeline)
        })
    }
}

func getRecord(with games: [Game]) -> String {
    var wins = 0
    var losses = 0
    var otLosses = 0
    var ties = 0
    
    for game in games {
        if game.shortResult != .Unknown {
            switch game.shortResult {
            case .Win:
                wins += 1
            case .Loss:
                losses += 1
            case .Tie:
                ties += 1
            case .OvertimeLoss:
                otLosses += 1
            default: break
            }
        }
    }
    
    return "\(wins)-\(losses)-\(otLosses)-\(ties)"
}

private var env: String {
    #if DEBUG
    return "http://127.0.0.1:8000"
    #elseif TEST
    return "https://test.gthockey.com"
    #else
    return "https://gthockey.com"
    #endif
}

func getData(completion: @escaping ([Game]) -> Void) {
    let endpoint = "\(env)/api/games/?fulldata"
    let session = URLSession(configuration: .default)
    session.dataTask(with: URL(string: endpoint)!) { (data, _, err) in
        if err != nil {
            print(err!.localizedDescription)
            return
        }
        
        do {
            let jsonData = try JSONDecoder().decode([Game].self, from: data!)
            completion(jsonData)
        } catch {
            print(error.localizedDescription)
        }
    }.resume()
}

struct SimpleEntry: TimelineEntry {
    let date: Date
    let widgetData: Game
    let widgetRecord: String
}

struct gthockey_iOS_widgetEntryView: View {
    var entry: Provider.Entry
        
    var body: some View {
        NextGameView(game: entry.widgetData, record: entry.widgetRecord)
            .foregroundColor(.white)
            .background(Color(UIColor.widgetBackgroundColor!))
            .widgetURL(URL(string: "schedule"))
    }
}

@main
struct gthockey_iOS_widget: Widget {
    let kind: String = "gthockey_iOS_widget"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            gthockey_iOS_widgetEntryView(entry: entry)
        }
        .configurationDisplayName("Next Game")
        .description("Countdown to Georgia Tech Hockey's next game.")
        .supportedFamilies([.systemSmall, .systemMedium])
    }
}

struct gthockey_iOS_widget_Previews: PreviewProvider {
    static var previews: some View {
        let testModel = GTHTestData().gameUpcomingTournament
        Group {
            gthockey_iOS_widgetEntryView(entry: SimpleEntry(date: Date(), widgetData: testModel, widgetRecord: "1-2-3-4"))
                .previewContext(WidgetPreviewContext(family: .systemSmall))
            gthockey_iOS_widgetEntryView(entry: SimpleEntry(date: Date(), widgetData: testModel, widgetRecord: "1-2-3-4"))
                .previewContext(WidgetPreviewContext(family: .systemMedium))
        }
    }
}
