//
//  CartTableViewCell.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 11/15/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//
//

import UIKit
import SDWebImage

class CartTableViewCell: UITableViewCell {

    // MARK: Properties

    private let productImageView: UIImageView = {
        let productImageView = UIImageView()
        productImageView.backgroundColor = .gray
        productImageView.clipsToBounds = true
        productImageView.contentMode = .scaleAspectFill
        productImageView.translatesAutoresizingMaskIntoConstraints = false
        return productImageView
    }()

    private let nameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.font = UIFont.Oswald.bold.font(size: 20)
        nameLabel.adjustsFontSizeToFitWidth = true
        nameLabel.numberOfLines = 1
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        return nameLabel
    }()

    private let attributesStack: UIStackView = {
        let attributesStack = UIStackView()
        attributesStack.axis = .vertical
        attributesStack.distribution = .fillEqually
        attributesStack.translatesAutoresizingMaskIntoConstraints = false
        return attributesStack
    }()

    private let priceLabel: UILabel = {
        let priceLabel = UILabel()
        priceLabel.font = UIFont.Oswald.regular.font(size: 16)
        priceLabel.adjustsFontSizeToFitWidth = true
        priceLabel.numberOfLines = 1
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        return priceLabel
    }()

    // MARK: Init

    override init(style: CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        backgroundColor = UIColor.gthBackgroundColor
        isUserInteractionEnabled = false
        contentView.addSubviews([productImageView, nameLabel, attributesStack, priceLabel])

        updateConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func updateConstraints() {
        super.updateConstraints()

        NSLayoutConstraint.activate([
            productImageView.topAnchor.constraint(greaterThanOrEqualTo: contentView.topAnchor, constant: 8.0),
            productImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.0),
            productImageView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -8.0),
            productImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            productImageView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.3),
            productImageView.heightAnchor.constraint(equalTo: productImageView.widthAnchor)
        ])

        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: productImageView.topAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: productImageView.trailingAnchor, constant: 8.0),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20.0)
        ])

        NSLayoutConstraint.activate([
            attributesStack.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8.0),
            attributesStack.leadingAnchor.constraint(equalTo: productImageView.trailingAnchor, constant: 8.0),
            attributesStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20.0)
        ])

        NSLayoutConstraint.activate([
            priceLabel.topAnchor.constraint(greaterThanOrEqualTo: attributesStack.bottomAnchor, constant: 8.0),
            priceLabel.leadingAnchor.constraint(equalTo: productImageView.trailingAnchor, constant: 8.0),
            priceLabel.bottomAnchor.constraint(lessThanOrEqualTo: productImageView.bottomAnchor)
        ])
    }

    // MARK: Setter

    public func set(with cartItem: CartItem) {
        productImageView.sd_setImage(with: cartItem.imageURL, placeholderImage: nil)
        nameLabel.text = cartItem.name
        attributesStack.removeAllArrangedSubviews()

        for attribute in cartItem.restrictedAttributes {
            let attributeLabel = UILabel()
            attributeLabel.font = UIFont.Oswald.regular.font(size: 12)
            attributeLabel.adjustsFontSizeToFitWidth = true
            attributeLabel.numberOfLines = 1
            attributeLabel.translatesAutoresizingMaskIntoConstraints = false
            attributeLabel.text = "\(attribute.key) - \(attribute.value)"
            attributesStack.addArrangedSubview(attributeLabel)
        }
        
        for attribute in cartItem.customAttributes {
            let attributeLabel = UILabel()
            attributeLabel.font = UIFont.Oswald.regular.font(size: 12)
            attributeLabel.adjustsFontSizeToFitWidth = true
            attributeLabel.numberOfLines = 1
            attributeLabel.translatesAutoresizingMaskIntoConstraints = false
            attributeLabel.text = "\(attribute.key) - \(attribute.value)"
            attributesStack.addArrangedSubview(attributeLabel)
        }

        priceLabel.text = cartItem.getPriceString()

        updateConstraints()
    }

}
