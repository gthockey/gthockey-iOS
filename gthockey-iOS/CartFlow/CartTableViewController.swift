//
//  CartTableViewController.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 11/15/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//

import UIKit
import Stripe
import DZNEmptyDataSet

class CartTableViewController: UITableViewController {
    
    // MARK: Properties
    
    private var footerHasBeenSet: Bool = false
    private var cartItems: [CartItem] = []
    private var paymentSheet: PaymentSheet?
    private var paymentContext: STPPaymentContext? = nil

    // MARK: Init

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Cart"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close,
                                                            target: self,
                                                            action: #selector(close))

        tableView.register(CartTableViewCell.self, forCellReuseIdentifier: "cartTableViewCell")
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        fetchCart()
        
        self.paymentContext?.hostViewController = self
    }
    
    // MARK: Private Functions
    
    @objc private func close() {
        dismiss(animated: true, completion: nil)
    }

    private func setupTableViewFooter() {
        if !footerHasBeenSet {
            let cartTableViewFooter = CartTableViewFooter()
            cartTableViewFooter.delegate = self
            let size = cartTableViewFooter.systemLayoutSizeFitting(CGSize(width: view.frame.width,
                                                                          height: UIView.layoutFittingCompressedSize.height))
            if cartTableViewFooter.frame.size.height != size.height {
                cartTableViewFooter.frame.size.height = size.height
                self.tableView.tableFooterView = cartTableViewFooter
            }
            footerHasBeenSet = true
        }
    }

    @objc private func fetchCart() {
        guard let rawCartItems = UserDefaults.standard.value(forKey: "cart") as? [[String : Any]],
              !rawCartItems.isEmpty
        else {
            tableView.tableFooterView = UIView()
            return
        }
        var totalPrice = 0.0
        var refinedCartItems = [CartItem]()
        for rawCartItem in rawCartItems {
            let rawRestrictedAttributes = rawCartItem["restrictedAttributes"] as! [[String : Any]]
            let rawCustomAttributes = rawCartItem["customAttributes"] as! [[String : Any]]
            
            var restrictedAttributes = [CartAttribute]()
            for attr in rawRestrictedAttributes {
                restrictedAttributes.append(CartAttribute(id: attr["id"] as! Int,
                                                          key: attr["key"] as! String,
                                                          value: attr["value"] as! String))
            }
            
            var customAttributes = [CartAttribute]()
            for attr in rawCustomAttributes {
                customAttributes.append(CartAttribute(id: attr["id"] as! Int,
                                                      key: attr["key"] as! String,
                                                      value: attr["value"] as! String))
            }
            
            let refinedCartItem = CartItem(id: rawCartItem["id"] as! Int,
                                           name: rawCartItem["name"] as! String,
                                           imageURL: URL(string: rawCartItem["imageURL"] as! String)!,
                                           price: rawCartItem["price"] as! Double,
                                           restrictedAttributes: restrictedAttributes,
                                           customAttributes: customAttributes)
            totalPrice += rawCartItem["price"] as! Double
            refinedCartItems.append(refinedCartItem)
        }
        
        self.cartItems = refinedCartItems
        self.paymentContext?.paymentAmount = Int(totalPrice)
        tableView.reloadData()
        setupTableViewFooter()
    }
    
    private func setupPaymentDrawer(with shippingInfo: ShippingInfo) {
        // Fetch the PaymentIntent and Customer information from the backend if user has something in their cart
        CartManager().setupPayment(with: shippingInfo, cartItems, completion: { paymentIntentClientSecret in
            guard let paymentIntentClientSecret = paymentIntentClientSecret else { return }
            
            var configuration = PaymentSheet.Configuration()
            configuration.merchantDisplayName = "GT Hockey"
            self.paymentSheet = PaymentSheet(paymentIntentClientSecret: paymentIntentClientSecret, configuration: configuration)
            DispatchQueue.main.async {
                self.checkout()
            }
        })
    }
    
    private func checkout() {
        paymentSheet?.present(from: self) { paymentResult in
            switch paymentResult {
            case .completed:
                print("Completed")
                UserDefaults.standard.setValue(nil, forKey: "cart")
                let alertController = UIAlertController(title: "Thank you for your order",
                                                        message: "You should be receiving an email receipt from us shortly.", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alertController, animated: true, completion: nil)
            case .canceled: break
            case .failed(let error):
                let alertController = UIAlertController(title: "Something went wrong",
                                                        message: "Error: \(error.localizedDescription)", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                alertController.addAction(UIAlertAction(title: "Contact Team", style: .default, handler: { _ in
                    let email = "mink.gthockey@gmail.com"
                    if let url = URL(string: "mailto:\(email)") {
                        UIApplication.shared.open(url)
                    }
                }))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }

    // MARK: Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItems.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartTableViewCell", for: indexPath) as! CartTableViewCell
        cell.set(with: cartItems[indexPath.row])
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 148.0
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if var rawCart = UserDefaults.standard.value(forKey: "cart") as? [[String : Any]] {
                rawCart.remove(at: indexPath.row)
                UserDefaults.standard.setValue(rawCart, forKey: "cart")
                cartItems = []
                tableView.reloadData()
                fetchCart()
            }
        }
    }

}

extension CartTableViewController: CartTableViewFooterDelegate {
    
    func payButtonTapped(with shippingInfo: ShippingInfo) {
        setupPaymentDrawer(with: shippingInfo)
    }

}

extension CartTableViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Nothing in your cart"
        let attrs = [NSAttributedString.Key.font: UIFont.Oswald.bold.font(size: 24)]
        return NSAttributedString(string: str, attributes: attrs)
    }

    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Head to the Shop tab to add items."
        let attrs = [NSAttributedString.Key.font: UIFont.Oswald.regular.font(size: 20)]
        return NSAttributedString(string: str, attributes: attrs)
    }

    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> NSAttributedString? {
        let str = "Go to Shop"
        let attrs = [NSAttributedString.Key.font: UIFont.Oswald.regular.font(size: 20)]
        return NSAttributedString(string: str, attributes: attrs)
    }

    func emptyDataSet(_ scrollView: UIScrollView, didTap button: UIButton) {
        PreLaunchViewController.gtTabBarController.selectedIndex = 3
        self.dismiss(animated: true, completion: nil)
    }
    
}
