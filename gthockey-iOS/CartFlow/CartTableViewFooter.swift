//
//  CartTableViewFooter.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 11/16/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//

import UIKit

struct ShippingInfo {
    var name: String
    var email: String
    var line1: String
    var line2: String
    var city: String
    var state: String
    var zip: String
}

protocol CartTableViewFooterDelegate {
    func payButtonTapped(with shippingInfo: ShippingInfo)
}

class CartTableViewFooter: UIView, UITextFieldDelegate {

    // MARK: Properties

    private let customerLabel: UILabel = {
        let customerLabel = UILabel()
        customerLabel.font = UIFont.Oswald.regular.font(size: 20.0)
        customerLabel.text = "Customer Info"
        customerLabel.adjustsFontSizeToFitWidth = true
        customerLabel.numberOfLines = 1
        customerLabel.textAlignment = .left
        customerLabel.translatesAutoresizingMaskIntoConstraints = false
        return customerLabel
    }()
    
    private let emailTextField: ShopOptionTextField = {
        let emailTextField = ShopOptionTextField()
        emailTextField.placeholder = "Email"
        emailTextField.keyboardType = .emailAddress
        emailTextField.autocapitalizationType = .none
        return emailTextField
    }()
    
    private let shippingLabel: UILabel = {
        let shippingLabel = UILabel()
        shippingLabel.font = UIFont.Oswald.regular.font(size: 20.0)
        shippingLabel.text = "Shipping Info"
        shippingLabel.adjustsFontSizeToFitWidth = true
        shippingLabel.numberOfLines = 1
        shippingLabel.textAlignment = .left
        shippingLabel.translatesAutoresizingMaskIntoConstraints = false
        return shippingLabel
    }()
    
    private let nameStack: UIStackView = {
        let nameStack = UIStackView()
        nameStack.axis = .horizontal
        nameStack.spacing = 4
        nameStack.distribution = .fillEqually
        nameStack.translatesAutoresizingMaskIntoConstraints = false
        return nameStack
    }()
    
    private let firstNameTextField: ShopOptionTextField = {
        let firstNameTextField = ShopOptionTextField()
        firstNameTextField.placeholder = "First Name"
        return firstNameTextField
    }()
    
    private let lastNameTextField: ShopOptionTextField = {
        let lastNameTextField = ShopOptionTextField()
        lastNameTextField.placeholder = "Last Name"
        return lastNameTextField
    }()
    
    private let line1TextField: ShopOptionTextField = {
        let line1TextField = ShopOptionTextField()
        line1TextField.placeholder = "Line 1"
        return line1TextField
    }()
    
    private let line2TextField: ShopOptionTextField = {
        let line2TextField = ShopOptionTextField()
        line2TextField.placeholder = "Line 2 (optional)"
        return line2TextField
    }()
    
    private let cityTextField: ShopOptionTextField = {
        let cityTextField = ShopOptionTextField()
        cityTextField.placeholder = "City"
        return cityTextField
    }()
    
    private let stateTextField: ShopOptionTextField = {
        let stateTextField = ShopOptionTextField()
        stateTextField.placeholder = "State"
        return stateTextField
    }()
    
    private let zipTextField: ShopOptionTextField = {
        let zipTextField = ShopOptionTextField()
        zipTextField.placeholder = "Zip"
        zipTextField.keyboardType = .numberPad
        return zipTextField
    }()
    
    private let payButton = PillButton(title: "Continue to Payment",
                                       backgroundColor: UIColor.shopDetailCTAColor!,
                                       borderColor: UIColor.shopDetailCTAColor!,
                                       isEnabled: false)
    
    public var delegate: CartTableViewFooterDelegate!

    // MARK: Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        emailTextField.delegate = self
        line1TextField.delegate = self
        line2TextField.delegate = self
        cityTextField.delegate = self
        stateTextField.delegate = self
        zipTextField.delegate = self
        
        payButton.addTarget(self, action: #selector(payButtonTapped), for: .touchUpInside)

        nameStack.addArrangedSubviews([firstNameTextField, lastNameTextField])
        addSubviews([customerLabel, emailTextField, shippingLabel, nameStack, line1TextField, line2TextField,
                     cityTextField, stateTextField, zipTextField, payButton])
        updateConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func updateConstraints() {
        super.updateConstraints()
        
        NSLayoutConstraint.activate([
            customerLabel.topAnchor.constraint(equalTo: topAnchor, constant: 12.0),
            customerLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            customerLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12.0),
        ])
        
        NSLayoutConstraint.activate([
            emailTextField.topAnchor.constraint(equalTo: customerLabel.bottomAnchor, constant: 12.0),
            emailTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            emailTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12.0),
        ])
        
        NSLayoutConstraint.activate([
            shippingLabel.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 24.0),
            shippingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            shippingLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12.0),
        ])
        
        NSLayoutConstraint.activate([
            nameStack.topAnchor.constraint(equalTo: shippingLabel.bottomAnchor, constant: 12.0),
            nameStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            nameStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12.0),
        ])
        
        NSLayoutConstraint.activate([
            line1TextField.topAnchor.constraint(equalTo: nameStack.bottomAnchor, constant: 12.0),
            line1TextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            line1TextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12.0),
        ])
        
        NSLayoutConstraint.activate([
            line2TextField.topAnchor.constraint(equalTo: line1TextField.bottomAnchor, constant: 12.0),
            line2TextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            line2TextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12.0),
        ])
        
        NSLayoutConstraint.activate([
            cityTextField.topAnchor.constraint(equalTo: line2TextField.bottomAnchor, constant: 12.0),
            cityTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            cityTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12.0),
        ])
        
        NSLayoutConstraint.activate([
            stateTextField.topAnchor.constraint(equalTo: cityTextField.bottomAnchor, constant: 12.0),
            stateTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            stateTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12.0),
        ])
        
        NSLayoutConstraint.activate([
            zipTextField.topAnchor.constraint(equalTo: stateTextField.bottomAnchor, constant: 12.0),
            zipTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            zipTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12.0),
        ])
        
        NSLayoutConstraint.activate([
            payButton.topAnchor.constraint(equalTo: zipTextField.bottomAnchor, constant: 24.0),
            payButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            payButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12.0),
            payButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -12.0)
        ])
    }

    // MARK: Action
    
    @objc private func payButtonTapped() {
        guard let firstName = firstNameTextField.text, firstName.count > 2,
              let lastName = lastNameTextField.text, lastName.count > 2,
              let email = emailTextField.text, email.count > 2, email.contains("@"),
              let line1 = line1TextField.text, line1.count > 3,
              let city = cityTextField.text, city.count > 2,
              let state = stateTextField.text, state.count > 1,
              let zip = zipTextField.text, zip.count == 5
        else {
            payButton.isEnabled = false
            return
        }
        payButton.isEnabled = true
        
        let shippingInfo = ShippingInfo(name: "\(firstName) \(lastName)",
                                        email: email,
                                        line1: line1,
                                        line2: line2TextField.text ?? "",
                                        city: city,
                                        state: state,
                                        zip: zip)
        delegate.payButtonTapped(with: shippingInfo)
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        guard let firstName = firstNameTextField.text, firstName.count > 2,
              let lastName = lastNameTextField.text, lastName.count > 2,
              let email = emailTextField.text, email.count > 2, email.contains("@"),
              let line1 = line1TextField.text, line1.count > 3,
              let city = cityTextField.text, city.count > 2,
              let state = stateTextField.text, state.count > 1,
              let zip = zipTextField.text, zip.count == 5
        else {
            payButton.isEnabled = false
            return
        }
        payButton.isEnabled = true
    }
    
}
