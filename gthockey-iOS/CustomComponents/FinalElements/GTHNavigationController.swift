//
//  GTHNavigationController.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 5/5/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import UIKit

final class GTHNavigationController: UINavigationController {

    // MARK: Init

    override func viewDidLoad() {
        super.viewDidLoad()
    
        delegate = self

        navigationBar.prefersLargeTitles = true
        navigationBar.barTintColor = UIColor.gthBackgroundColor
        navigationBar.tintColor = UIColor.gthNavigationControllerTintColor

        navigationBar.largeTitleTextAttributes = [
            .foregroundColor: UIColor.gthNavigationControllerTintColor as Any,
            .font: UIFont.Oswald.medium.font(size: 48.0)
        ]
        navigationBar.titleTextAttributes = [
            .foregroundColor: UIColor.gthNavigationControllerTintColor as Any,
            .font: UIFont.Oswald.medium.font(size: 24.0)
        ]
    }
    
    // MARK: Private Functions
    
    @objc private func openCart() {
        let cartTableViewController = CartTableViewController()
        cartTableViewController.view.backgroundColor = UIColor.gthBackgroundColor
        let cartNavigationController = UINavigationController(rootViewController: cartTableViewController)
        cartNavigationController.navigationBar.titleTextAttributes = [
            .foregroundColor: UIColor.gthNavigationControllerTintColor as Any,
            .font: UIFont.Oswald.medium.font(size: 24.0)
        ]
        present(cartNavigationController, animated: true, completion: nil)
    }

}

extension GTHNavigationController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        viewController.view.backgroundColor = UIColor.gthBackgroundColor
        viewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        viewController.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "cart"),
                                                                           style: .plain,
                                                                           target: self,
                                                                           action: #selector(openCart))
        
        if viewController as? NewsCollectionViewController != nil {
            navigationBar.topItem?.title = "News"
        } else if viewController as? RosterCollectionViewController != nil {
            navigationBar.topItem?.title = "Roster"
        } else if viewController as? ShopCollectionViewController != nil {
            navigationBar.topItem?.title = "Shop"
        }
    }
    
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        if viewController as? MoreTableViewController != nil {
            navigationBar.topItem?.title = "More"
        }
    }
    
}
