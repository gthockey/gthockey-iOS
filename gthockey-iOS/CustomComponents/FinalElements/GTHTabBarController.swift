//
//  GTHTabBarController.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 5/5/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import UIKit
import SwiftUI

final class GTHTabBarController: UITabBarController {

    // MARK: Properties

    private let indicatorPlatform = UIView()

    private let newsCollectionViewController: NewsCollectionViewController = {
        let newsLayout = UICollectionViewFlowLayout()
        newsLayout.sectionInset = UIEdgeInsets(top: 12.0, left: 0.0, bottom: 24.0, right: 0.0)
        let newsCollectionViewController = NewsCollectionViewController(collectionViewLayout: newsLayout)
        return newsCollectionViewController
    }()
    
    private let scheduleTabBarItem = UITabBarItem(title: nil, image: UIImage(systemName: "calendar"), tag: 1)

    private let rosterCollectionViewController: RosterCollectionViewController = {
        let rosterLayout = UICollectionViewFlowLayout()
        rosterLayout.sectionInset = UIEdgeInsets(top: 12.0, left: 0.0, bottom: 24.0, right: 0.0)
        let rosterCollectionViewController = RosterCollectionViewController(collectionViewLayout: rosterLayout)
        return rosterCollectionViewController
    }()

    private let shopCollectionViewController: ShopCollectionViewController = {
        let shopLayout = UICollectionViewFlowLayout()
        shopLayout.sectionInset = UIEdgeInsets(top: 12.0, left: 0.0, bottom: 24.0, right: 0.0)
        let shopCollectionViewController = ShopCollectionViewController(collectionViewLayout: shopLayout)
        return shopCollectionViewController
    }()
    
    private let moreTableViewController = MoreTableViewController()
    
    // MARK: Override Variables
    
    override var selectedIndex: Int {
        didSet {
            moveIndicatorPlatform(to: selectedIndex, animated: false)
        }
    }

    // MARK: Init

    override func viewDidLoad() {
        super.viewDidLoad()

        let newsNavigationController = GTHNavigationController(rootViewController: newsCollectionViewController)
        newsNavigationController.tabBarItem = UITabBarItem(title: nil, image: UIImage(systemName: "house"), tag: 0)

        let scheduleNavigationController = GTHNavigationController(rootViewController: UIHostingController(rootView: ScheduleList()))
        scheduleNavigationController.tabBarItem = scheduleTabBarItem
        
        let rosterNavigationController = GTHNavigationController(rootViewController: rosterCollectionViewController)
        rosterNavigationController.tabBarItem = UITabBarItem(title: nil, image: UIImage(systemName: "person.2"), tag: 2)

        let shopNavigatinController = GTHNavigationController(rootViewController: shopCollectionViewController)
        shopNavigatinController.tabBarItem = UITabBarItem(title: nil, image: UIImage(systemName: "bag"), tag: 3)

        let moreNavigationController = GTHNavigationController(rootViewController: moreTableViewController)
        moreNavigationController.tabBarItem = UITabBarItem(title: nil, image: UIImage(systemName: "line.horizontal.3"), tag: 4)

        setViewControllers([
            newsNavigationController,
            scheduleNavigationController,
            rosterNavigationController,
            shopNavigatinController,
            moreNavigationController
        ], animated: true)

        setupIndicatorPlatform()

        tabBar.barTintColor = UIColor.gthBackgroundColor
        tabBar.tintColor = UIColor.gthTabBarControllerTintColor
        tabBar.isTranslucent = false
    }

    // MARK: UITabBarDelegate

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        moveIndicatorPlatform(to: tabBar.items!.firstIndex(of: item)!)
    }

    // MARK: Private Functions

    private func setupIndicatorPlatform() {
        let tabBarItemSize = CGSize(width: tabBar.frame.width / CGFloat(tabBar.items!.count), height: tabBar.frame.height)
        indicatorPlatform.backgroundColor = UIColor.gthTabBarControllerTintColor
        indicatorPlatform.frame = CGRect(x: 0.0, y: 0.0, width: tabBarItemSize.width, height: 2.0)
        indicatorPlatform.center.x = tabBar.frame.width / CGFloat(tabBar.items!.count) / 2.0
        tabBar.addSubview(indicatorPlatform)
    }
    
    private func moveIndicatorPlatform(to index: Int, animated: Bool = true) {
        let index = CGFloat(integerLiteral: index)
        let itemWidth = indicatorPlatform.frame.width
        let newCenterX = (itemWidth / 2.0) + (itemWidth * index)

        if animated {
            UIView.animate(withDuration: 0.3) {
                self.indicatorPlatform.center.x = newCenterX
            }
        } else {
            self.indicatorPlatform.center.x = newCenterX
        }
    }

}
