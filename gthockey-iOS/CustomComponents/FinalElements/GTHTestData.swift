//
//  GTHTestData.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 10/16/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import Foundation

class GTHTestData {
    
    // MARK: Opponents
    
    static let opponentUCF = Team(id: 38,
                           schoolName: "UCF",
                           mascotName: "Knights",
                           webURL: URL(string: "http://www.ucfhockey.com/"),
                           logoImageURL: URL(string: "https://prod.gthockey.com/media/teamlogos/UCF.png")!,
                           backgroundImageURL: URL(string: "https://prod.gthockey.com/media/teambackgrounds/UCFZoomed.png")!
    )
    
    static let opponentUGA = Team(id: 0,
                                  schoolName: "Georgia",
                                  mascotName: "Bulldogs",
                                  webURL: URL(string: "http://ugahockey.com"),
                                  logoImageURL: URL(string: "https://prod.gthockey.com/media/teamlogos/UGA.png")!,
                                  backgroundImageURL: URL(string: "https://prod.gthockey.com/media/teambackgrounds/UGAZoomed.png")!
    )
    
    static let opponentKSU = Team(id: 16,
                                  schoolName: "Kennesaw State",
                                  mascotName: "Owls",
                                  webURL: URL(string: "http://www.ksuicehockey.com"),
                                  logoImageURL: URL(string: "https://prod.gthockey.com/media/teamlogos/KSU.png")!,
                                  backgroundImageURL: URL(string: "https://prod.gthockey.com/media/teambackgrounds/UCFZoomed.png")!
    )
    
    static let opponentUAH = Team(id: 46,
                                  schoolName: "UAH",
                                  mascotName: "Chargers",
                                  webURL: URL(string: "https://www.uahclubhockey.com/"),
                                  logoImageURL: URL(string: "https://prod.gthockey.com/media/teamlogos/UAH.png")!,
                                  backgroundImageURL: URL(string: "https://prod.gthockey.com/media/teambackgrounds/UAHZoomed.png")!
    )
    
    static let opponentClemson = Team(id: 3,
                                      schoolName: "Clemson",
                                      mascotName: "Tigers",
                                      webURL: URL(string: "http://clemsonthockey.pointstreaksites.com/view/clemsonhockey"),
                                      logoImageURL: URL(string: "https://prod.gthockey.com/media/teamlogos/Clemson.png")!,
                                      backgroundImageURL: URL(string: "https://prod.gthockey.com/media/teambackgrounds/ClemsonZoomed.png")!
    )
    
    // MARK: Rinks
    
    static let rinkAtlantaIceHouse = Rink(id: 0,
                                          name: "Atlanta Ice House"
    )
    
    static let rinkSavannahCivicCenter = Rink(id: 6,
                                              name: "Savannah Civic Center"
    )

    static let rinkBentonHWilcoxonMunicipalIceComplex = Rink(id: 30,
                                                             name: "Benton H. Wilcoxon Municipal Ice Complex"
    )
    
    static let rinkClassicCenter = Rink(id: 4,
                                        name: "The Classic Center"
    )
    
    // MARK: Seasons
    
    static let season2020 = Season(id: 9,
                                   name: "2020-21 Season",
                                   year: 2020
    )
    
    static let season2019 = Season(id: 8,
                                   name: "2019-20 Season",
                                   year: 2019
    )
    
    static let season2014 = Season(id: 5,
                                   name: "2014-15 Season",
                                   year: 2014
    )
    
    // MARK: Upcoming Games
    
    let gameUpcomingHome = Game(id: 207,
                                date: "2021-1-29",
                                time: "12:00:00",
                                opponent: opponentClemson,
                                venue: .Home,
                                rink: rinkAtlantaIceHouse,
                                season: season2020,
                                gtScore: nil,
                                opponentScore: nil,
                                shortResult: .Unknown
    )
    
    let gameUpcomingAway = Game(id: 209,
                                date: "2020-1-30",
                                time: "14:00:00",
                                opponent: opponentUAH,
                                venue: .Away,
                                rink: rinkBentonHWilcoxonMunicipalIceComplex,
                                season: season2020,
                                gtScore: nil,
                                opponentScore: nil,
                                shortResult: .Unknown
    )
    
    let gameUpcomingTournament = Game(id: 208,
                                      date: "2022-01-15",
                                      time: "20:00:00",
                                      opponent: opponentUGA,
                                      venue: .Tournament,
                                      rink: rinkSavannahCivicCenter,
                                      season: season2020,
                                      gtScore: nil,
                                      opponentScore: nil,
                                      shortResult: .Unknown
    )
    
    // MARK: Completed Games
    
    let gameCompletedHomeWin = Game(id: 160,
                                    date: "2019-10-11",
                                    time: "20:45:00",
                                    opponent: opponentUCF,
                                    venue: .Home,
                                    rink: rinkAtlantaIceHouse,
                                    season: season2019,
                                    gtScore: 6,
                                    opponentScore: 1,
                                    shortResult: .Win
    )
    
    let gameCompletedHomeLoss = Game(id: 165,
                                    date: "2019-11-01",
                                    time: "20:30:00",
                                    opponent: opponentKSU,
                                    venue: .Home,
                                    rink: rinkAtlantaIceHouse,
                                    season: season2019,
                                    gtScore: 1,
                                    opponentScore: 2,
                                    shortResult: .Loss
    )
    
    let gameCompletedAwayWin = Game(id: 157,
                                    date: "2019-09-27",
                                    time: "22:00:00",
                                    opponent: opponentUAH,
                                    venue: .Away,
                                    rink: rinkBentonHWilcoxonMunicipalIceComplex,
                                    season: season2019,
                                    gtScore: 8,
                                    opponentScore: 1,
                                    shortResult: .Win
    )
    
    let gameCompletedAwayLoss = Game(id: 162,
                                    date: "2019-10-18",
                                    time: "19:30:00",
                                    opponent: opponentUGA,
                                    venue: .Away,
                                    rink: rinkClassicCenter,
                                    season: season2019,
                                    gtScore: 2,
                                    opponentScore: 5,
                                    shortResult: .Loss
    )
    
    let gameCompletedTournamentWin = Game(id: 141,
                                          date: "2015-01-17",
                                          time: "20:30:00",
                                          opponent: opponentUGA,
                                          venue: .Tournament,
                                          rink: rinkSavannahCivicCenter,
                                          season: season2014,
                                          gtScore: 5,
                                          opponentScore: 4,
                                          shortResult: .Win
    )
    
    let gameCompletedTournamentLoss = Game(id: 173,
                                           date: "2020-01-18",
                                           time: "20:30:00",
                                           opponent: opponentUGA,
                                           venue: .Tournament,
                                           rink: rinkSavannahCivicCenter,
                                           season: season2019,
                                           gtScore: 3,
                                           opponentScore: 4,
                                           shortResult: .OvertimeLoss
    )
    
}
