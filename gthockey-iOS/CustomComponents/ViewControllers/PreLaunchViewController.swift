//
//  PreLaunchViewController.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 11/13/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//

import UIKit
import RevealingSplashView

enum FirstScreenOnLaunch {
    case home
    case schedule
}

class PreLaunchViewController: UIViewController {

    // MARK: Properties

    static let gtTabBarController = GTHTabBarController()
    
    private let appIconManager = AppIconManager()
    private var revealingSplashView: RevealingSplashView?
    private let firstScreen: FirstScreenOnLaunch
    
    // MARK: Init
    
    init(firstScreen: FirstScreenOnLaunch = .home) {
        self.firstScreen = firstScreen
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        revealingSplashView = RevealingSplashView(iconImage: UIImage(named: appIconManager.launchLogo)!,
                                                  iconInitialSize: CGSize(width: 124.0, height: 124.0),
                                                  backgroundColor: appIconManager.launchBackground)
        revealingSplashView?.alpha = 0.0
        view.addSubview(revealingSplashView!)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.3,
                       animations: {
                        self.revealingSplashView?.alpha = 1.0
        }, completion: { _ in
            self.checkLoginStatus()
        })
    }

    // MARK: Status Functions

    private func checkLoginStatus() {
        switch self.firstScreen {
        case .schedule: PreLaunchViewController.gtTabBarController.selectedIndex = 1
        default: break
        }
        PreLaunchViewController.gtTabBarController.modalPresentationStyle = .fullScreen
        PreLaunchViewController.gtTabBarController.modalTransitionStyle = .crossDissolve
        self.revealingSplashView?.startAnimation({
            self.present(PreLaunchViewController.gtTabBarController, animated: false, completion: nil)
        })
    }

}
