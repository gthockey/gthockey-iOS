//
//  UIColorExtension.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 10/22/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static let techNavy = UIColor(red: 37/255, green: 47/255, blue: 86/255, alpha: 1.0)
    static let techGold = UIColor(red: 213/255, green: 193/255, blue: 104/255, alpha: 1.0)
    static let techCream = UIColor(red: 225/255, green: 212/255, blue: 159/255, alpha: 1.0)
    
    static let gthBackgroundColor = UIColor.init(named: "GTHBackgroundColor")
    static let gthNavigationControllerTintColor = UIColor.init(named: "GTHNavigationControllerTintColor")
    static let gthTabBarControllerTintColor = UIColor.init(named: "GTHTabBarControllerTintColor")
    
    static let newsCellDateColor = UIColor.init(named: "NewsCellDateColor")
    static let newsCellTitleColor = UIColor.init(named: "NewsCellTitleColor")
    
    static let newsDetailDateColor = UIColor.init(named: "NewsDetailDateColor")
    static let newsDetailTitleColor = UIColor.init(named: "NewsDetailTitleColor")
    static let newsDetailContentColor = UIColor.init(named: "NewsDetailContentColor")
    
    static let scheduleCellTeamInfoColor = UIColor.init(named: "ScheduleCellTeamInfoColor")
    static let scheduleCellGameInfoColor = UIColor.init(named: "ScheduleCellGameInfoColor")
    
    static let scheduleHeaderTitleColor = UIColor.init(named: "ScheduleHeaderTitleColor")
    static let scheduleHeaderRecordColor = UIColor.init(named: "ScheduleHeaderRecordColor")
    
    static let rosterCellNameColor = UIColor.init(named: "RosterCellNameColor")
    static let rosterCellNumberColor = UIColor.init(named: "RosterCellNumberColor")
    
    static let rosterHeaderTitleColor = UIColor.init(named: "RosterHeaderTitleColor")
    
    static let shopCellTitleColor = UIColor.init(named: "ShopCellTitleColor")
    static let shopCellPriceColor = UIColor.init(named: "ShopCellPriceColor")
    
    static let rosterDetailFirstNameColor = UIColor.init(named: "RosterDetailFirstNameColor")
    static let rosterDetailLastNameColor = UIColor.init(named: "RosterDetailLastNameColor")
    static let rosterDetailPositionColor = UIColor.init(named: "RosterDetailPositionColor")
    static let rosterDetailNumberColor = UIColor.init(named: "RosterDetailNumberColor")
    static let rosterDetailHometownColor = UIColor.init(named: "RosterDetailHometownColor")
    static let rosterDetailSchoolColor = UIColor.init(named: "RosterDetailSchoolColor")
    static let rosterDetailBioColor = UIColor.init(named: "RosterDetailBioColor")
    
    static let shopDetailTitleColor = UIColor.init(named: "ShopDetailTitleColor")
    static let shopDetailPriceColor = UIColor.init(named: "ShopDetailPriceColor")
    static let shopDetailContentColor = UIColor.init(named: "ShopDetailContentColor")
    static let shopDetailCTAColor = UIColor.init(named: "ShopDetailCTAColor")
    
    static let boardMemberCellNameColor = UIColor.init(named: "BoardMemberCellNameColor")
    static let coachingStaffCellNameColor = UIColor.init(named: "CoachingStaffCellNameColor")
    
    static let moreCellBackgroundColor = UIColor.init(named: "MoreCellBackgroundColor")
    static let moreCellTitleColor = UIColor.init(named: "MoreCellTitleColor")
    static let moreHeaderBackgroundColor = UIColor.init(named: "MoreHeaderBackgroundColor")
    static let moreHeaderTitleColor = UIColor.init(named: "MoreHeaderTitleColor")
    
    static let boardAndCoachesEmailButtonColor = UIColor.init(named: "BoardAndCoachesEmailButtonColor")
    
    static let widgetBackgroundColor = UIColor.init(named: "WidgetBackgroundColor")
}
