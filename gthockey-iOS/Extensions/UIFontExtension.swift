//
//  UIFontExtension.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 5/5/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

extension UIFont {
    
    public enum Oswald: String {

        /* Oswald Font Options:
         - Regular -> "Oswald-Regular"
         - ExtraLight -> "Oswald-Regular_ExtraLight"
         - Light -> "Oswald-Regular_Light"
         - Medium -> "Oswald-Regular_Medium"
         - SemiBold -> "Oswald-Regular_SemiBold"
         - Bold -> "Oswald-Regular_Bold"
        */
        
        case regular = "Oswald-Regular"
        case extraLight = "Oswald-Regular_ExtraLight"
        case light = "Oswald-Regular_Light"
        case medium = "Oswald-Regular_Medium"
        case semiBold = "Oswald-Regular_SemiBold"
        case bold = "Oswald-Regular_Bold"

        public func font(size: CGFloat) -> UIFont {
            return UIFont(name: self.rawValue, size: size)!
        }
    }
    
}
