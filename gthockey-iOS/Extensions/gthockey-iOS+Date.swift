//
//  gthockey-iOS+Date.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 10/19/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import Foundation

extension Date {
    
    // MARK: Date Only
    
    /// Date: Short `(11/23/37)`
    /// Time: None
    var shortDateOnly: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none
        return formatter.string(from: self)
    }
    
    /// Date: Medium `(Nov 23, 1937)`
    /// Time: None
    var mediumDateOnly: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter.string(from: self)
    }
    
    /// Date: Long `(November 23, 1937)`
    /// Time: None
    var longDateOnly: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        return formatter.string(from: self)
    }
    
    /// Date: Short `(Tuesday, April 12, 1952 AD)`
    /// Time: None
    var fullDateOnly: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .none
        return formatter.string(from: self)
    }
    
    // MARK: Date + Short Time
    
    /// Date: Short `(11/23/37)`
    /// Time: Short `(3:30 PM)`
    var shortDateShortTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
    
    /// Date: Medium `(Nov 23, 1937)`
    /// Time: Short `(3:30 PM)`
    var mediumDateShortTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
    
    /// Date: Long `(November 23, 1937)`
    /// Time: Short `(3:30 PM)`
    var longDateShortTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
    
    /// Date: Short `(Tuesday, April 12, 1952 AD)`
    /// Time: Short `(3:30 PM)`
    var fullDateShortTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
    
    // MARK: Date + Medium Time
    
    /// Date: Short `(11/23/37)`
    /// Time: Medium `(3:30:32 PM)`
    var shortDateMediumTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .medium
        return formatter.string(from: self)
    }
    
    /// Date: Medium `(Nov 23, 1937)`
    /// Time: Medium `(3:30:32 PM)`
    var mediumDateMediumTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        return formatter.string(from: self)
    }
    
    /// Date: Long `(November 23, 1937)`
    /// Time: Medium `(3:30:32 PM)`
    var longDateMediumTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .medium
        return formatter.string(from: self)
    }
    
    /// Date: Short `(Tuesday, April 12, 1952 AD)`
    /// Time: Medium `(3:30:32 PM)`
    var fullDateMediumTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .medium
        return formatter.string(from: self)
    }
    
    // MARK: Date + Long Time
    
    /// Date: Short `(11/23/37)`
    /// Time: Long `(3:30:32 PM PST)`
    var shortDateLongTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .long
        return formatter.string(from: self)
    }
    
    /// Date: Medium `(Nov 23, 1937)`
    /// Time: Long `(3:30:32 PM PST)`
    var mediumDateLongTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .long
        return formatter.string(from: self)
    }
    
    /// Date: Long `(November 23, 1937)`
    /// Time: Long `(3:30:32 PM PST)`
    var longDateLongTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .long
        return formatter.string(from: self)
    }
    
    /// Date: Short `(Tuesday, April 12, 1952 AD)`
    /// Time: Long `(3:30:32 PM PST)`
    var fullDateLongTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .long
        return formatter.string(from: self)
    }
    
    // MARK: Date + Full Time
    
    /// Date: Short `(11/23/37)`
    /// Time: Full `(3:30:42 PM Pacific Standard Time)`
    var shortDateFullTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .full
        return formatter.string(from: self)
    }
    
    /// Date: Medium `(Nov 23, 1937)`
    /// Time: Full `(3:30:42 PM Pacific Standard Time)`
    var mediumDateFullTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .full
        return formatter.string(from: self)
    }
    
    /// Date: Long `(November 23, 1937)`
    /// Time: Full `(3:30:42 PM Pacific Standard Time)`
    var longDateFullTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .full
        return formatter.string(from: self)
    }
    
    /// Date: Short `(Tuesday, April 12, 1952 AD)`
    /// Time: Full `(3:30:42 PM Pacific Standard Time)`
    var fullDateFullTime: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .full
        return formatter.string(from: self)
    }
    
    // MARK: Time Only
    
    /// Date: None
    /// Time: Short `(3:30 PM)`
    var shortTimeOnly: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
    
    /// Date: None
    /// Time: Long `(3:30:32 PM PST)`
    var mediumTimeOnly: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .medium
        return formatter.string(from: self)
    }
    
    /// Date: None
    /// Time: Long `(3:30:32 PM PST)`
    var longTimeOnly: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .long
        return formatter.string(from: self)
    }
    
    /// Date: None
    /// Time: Full `(3:30:42 PM Pacific Standard Time)`
    var fullTimeOnly: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .full
        return formatter.string(from: self)
    }
    
}
