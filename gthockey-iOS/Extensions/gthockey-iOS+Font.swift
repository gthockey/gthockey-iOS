//
//  gthockey-iOS+Font.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 10/16/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import SwiftUI

extension Font {
    
    // MARK: Oswald-Regular

    /// Font: Oswald-Regular
    /// Size: 8.0
    static var regularSmaller: Font {
        Font.custom("Oswald-Regular", size: 8.0)
    }
    
    /// Font: Oswald-Regular
    /// Size: 12.0
    static var regularSmall: Font {
        Font.custom("Oswald-Regular", size: 12.0)
    }
    
    /// Font: Oswald-Regular
    /// Size: 16.0
    static var regularMedium: Font {
        Font.custom("Oswald-Regular", size: 16.0)
    }
    
    /// Font: Oswald-Regular
    /// Size: 20.0
    static var regularLarge: Font {
        Font.custom("Oswald-Regular", size: 20.0)
    }
    
    /// Font: Oswald-Regular
    /// Size: 24.0
    static var regularLarger: Font {
        Font.custom("Oswald-Regular", size: 24.0)
    }
    
    /// Font: Oswald-Regular
    /// Size: 32.0
    static var regularExtraLarge: Font {
        Font.custom("Oswald-Regular", size: 32.0)
    }
    
    /// Font: Oswald-Regular
    /// Size: 48.0
    static var regularExtraExtraLarge: Font {
        Font.custom("Oswald-Regular", size: 48.0)
    }
    
    // MARK: Oswald-Regular_ExtraLight
    
    /// Font: Oswald-Regular_ExtraLight
    /// Size: 8.0
    static var extraLightSmaller: Font {
        Font.custom("Oswald-Regular_ExtraLight", size: 8.0)
    }
    
    /// Font: Oswald-Regular_ExtraLight
    /// Size: 12.0
    static var extraLightSmall: Font {
        Font.custom("Oswald-Regular_ExtraLight", size: 12.0)
    }
    
    /// Font: Oswald-Regular_ExtraLight
    /// Size: 16.0
    static var extraLightMedium: Font {
        Font.custom("Oswald-Regular_ExtraLight", size: 16.0)
    }
    
    /// Font: Oswald-Regular_ExtraLight
    /// Size: 20.0
    static var extraLightLarge: Font {
        Font.custom("Oswald-Regular_ExtraLight", size: 20.0)
    }
    
    /// Font: Oswald-Regular_ExtraLight
    /// Size: 24.0
    static var extraLightLarger: Font {
        Font.custom("Oswald-Regular_ExtraLight", size: 24.0)
    }
    
    /// Font: Oswald-Regular_ExtraLight
    /// Size: 32.0
    static var extraLightExtraLarge: Font {
        Font.custom("Oswald-Regular_ExtraLight", size: 32.0)
    }
    
    /// Font: Oswald-Regular_ExtraLight
    /// Size: 48.0
    static var extraLightExtraExtraLarge: Font {
        Font.custom("Oswald-Regular_ExtraLight", size: 48.0)
    }
    
    // MARK: Oswald-Regular_Light
    
    /// Font: Oswald-Regular_Light
    /// Size: 8.0
    static var lightSmaller: Font {
        Font.custom("Oswald-Regular_Light", size: 8.0)
    }
    
    /// Font: Oswald-Regular_Light
    /// Size: 12.0
    static var lightSmall: Font {
        Font.custom("Oswald-Regular_Light", size: 12.0)
    }
    
    /// Font: Oswald-Regular_Light
    /// Size: 16.0
    static var lightMedium: Font {
        Font.custom("Oswald-Regular_Light", size: 16.0)
    }
    
    /// Font: Oswald-Regular_Light
    /// Size: 20.0
    static var lightLarge: Font {
        Font.custom("Oswald-Regular_Light", size: 20.0)
    }
    
    /// Font: Oswald-Regular_Light
    /// Size: 24.0
    static var lightLarger: Font {
        Font.custom("Oswald-Regular_Light", size: 24.0)
    }
    
    /// Font: Oswald-Regular_Light
    /// Size: 32.0
    static var lightExtraLarge: Font {
        Font.custom("Oswald-Regular_Light", size: 32.0)
    }
    
    /// Font: Oswald-Regular_Light
    /// Size: 48.0
    static var lightExtraExtraLarge: Font {
        Font.custom("Oswald-Regular_Light", size: 48.0)
    }
    
    // MARK: Oswald-Regular_Medium
    
    /// Font: Oswald-Regular_Medium
    /// Size: 8.0
    static var mediumSmaller: Font {
        Font.custom("Oswald-Regular_Medium", size: 8.0)
    }
    
    /// Font: Oswald-Regular_Medium
    /// Size: 12.0
    static var mediumSmall: Font {
        Font.custom("Oswald-Regular_Medium", size: 12.0)
    }
    
    /// Font: Oswald-Regular_Medium
    /// Size: 16.0
    static var mediumMedium: Font {
        Font.custom("Oswald-Regular_Medium", size: 16.0)
    }
    
    /// Font: Oswald-Regular_Medium
    /// Size: 20.0
    static var mediumLarge: Font {
        Font.custom("Oswald-Regular_Medium", size: 20.0)
    }
    
    /// Font: Oswald-Regular_Medium
    /// Size: 24.0
    static var mediumLarger: Font {
        Font.custom("Oswald-Regular_Medium", size: 24.0)
    }
    
    /// Font: Oswald-Regular_Medium
    /// Size: 32.0
    static var mediumExtraLarge: Font {
        Font.custom("Oswald-Regular_Medium", size: 32.0)
    }
    
    /// Font: Oswald-Regular_Medium
    /// Size: 48.0
    static var mediumExtraExtraLarge: Font {
        Font.custom("Oswald-Regular_Medium", size: 48.0)
    }
    
    // MARK: Oswald-Regular_SemiBold
    
    /// Font: Oswald-Regular_SemiBold
    /// Size: 8.0
    static var semiBoldSmaller: Font {
        Font.custom("Oswald-Regular_SemiBold", size: 8.0)
    }
    
    /// Font: Oswald-Regular_SemiBold
    /// Size: 12.0
    static var semiBoldSmall: Font {
        Font.custom("Oswald-Regular_SemiBold", size: 12.0)
    }
    
    /// Font: Oswald-Regular_SemiBold
    /// Size: 16.0
    static var semiBoldMedium: Font {
        Font.custom("Oswald-Regular_SemiBold", size: 16.0)
    }
    
    /// Font: Oswald-Regular_SemiBold
    /// Size: 20.0
    static var semiBoldLarge: Font {
        Font.custom("Oswald-Regular_SemiBold", size: 20.0)
    }
    
    /// Font: Oswald-Regular_SemiBold
    /// Size: 24.0
    static var semiBoldLarger: Font {
        Font.custom("Oswald-Regular_SemiBold", size: 24.0)
    }
    
    /// Font: Oswald-Regular_SemiBold
    /// Size: 32.0
    static var semiBoldExtraLarge: Font {
        Font.custom("Oswald-Regular_SemiBold", size: 32.0)
    }
    
    /// Font: Oswald-Regular_SemiBold
    /// Size: 48.0
    static var semiBoldExtraExtraLarge: Font {
        Font.custom("Oswald-Regular_SemiBold", size: 48.0)
    }
    
    // MARK: Oswald-Regular_Bold
    
    /// Font: Oswald-Regular_Bold
    /// Size: 8.0
    static var boldSmaller: Font {
        Font.custom("Oswald-Regular_Bold", size: 8.0)
    }
    
    /// Font: Oswald-Regular_Bold
    /// Size: 12.0
    static var boldSmall: Font {
        Font.custom("Oswald-Regular_Bold", size: 12.0)
    }
    
    /// Font: Oswald-Regular_Bold
    /// Size: 16.0
    static var boldMedium: Font {
        Font.custom("Oswald-Regular_Bold", size: 16.0)
    }
    
    /// Font: Oswald-Regular_Bold
    /// Size: 20.0
    static var boldLarge: Font {
        Font.custom("Oswald-Regular_Bold", size: 20.0)
    }
    
    /// Font: Oswald-Regular_Bold
    /// Size: 24.0
    static var boldLarger: Font {
        Font.custom("Oswald-Regular_Bold", size: 24.0)
    }
    
    /// Font: Oswald-Regular_Bold
    /// Size: 32.0
    static var boldExtraLarge: Font {
        Font.custom("Oswald-Regular_Bold", size: 32.0)
    }
    
    /// Font: Oswald-Regular_Bold
    /// Size: 48.0
    static var boldExtraExtraLarge: Font {
        Font.custom("Oswald-Regular_Bold", size: 48.0)
    }
    
}
