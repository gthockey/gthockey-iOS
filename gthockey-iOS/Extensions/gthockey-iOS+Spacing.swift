//
//  gthockey-iOS+Spacing.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 10/17/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import CoreGraphics

public extension CGFloat {
    
    /// Size: 4.0
    static var smallest: CGFloat { 4.0 }
    
    /// Size: 8.0
    static var smaller: CGFloat { 8.0 }
    
    /// Size: 12.0
    static var small: CGFloat { 12.0 }
    
    /// Size: 16.0
    static var medium: CGFloat { 16.0 }
    
    /// Size: 20.0
    static var large: CGFloat { 20.0 }
    
    /// Size: 24.0
    static var larger: CGFloat { 24.0 }
    
    /// Size: 32.0
    static var extraLarge: CGFloat { 32.0 }
    
    /// Size: 48.0
    static var extraExtraLarge: CGFloat { 48.0 }
    
}
