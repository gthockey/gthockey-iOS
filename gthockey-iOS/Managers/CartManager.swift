//
//  CartManager.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 4/5/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import Foundation

class CartManager: NetworkingManager {

    // MARK: Init

    override init() {}

    // MARK: Public Functions
    
    func setupPayment(with shippingInfo: ShippingInfo, _ cart: [CartItem], completion: @escaping (String?) -> Void) {
        var params = [String : Any]()
        params["name"] = shippingInfo.name
        params["email"] = shippingInfo.email
        
        var addressDict = [String : Any]()
        addressDict["line1"] = shippingInfo.line1
        addressDict["line2"] = shippingInfo.line2
        addressDict["city"] = shippingInfo.city
        addressDict["state"] = shippingInfo.state
        addressDict["zip"] = shippingInfo.zip
        params["address"] = addressDict
        
        var cartArray = [[String: Any]]()
        for item in cart {
            var itemDict = [String: Any]()
            itemDict["item_id"] = item.id
            
            var restrictedAttributes = [[String : Any]]()
            for attr in item.restrictedAttributes {
                restrictedAttributes.append(["id": attr.id, "value": attr.value])
            }
            itemDict["options"] = restrictedAttributes
            
            var customAttributes = [[String : Any]]()
            for attr in item.customAttributes {
                customAttributes.append(["id": attr.id, "value": attr.value])
            }
            itemDict["custom_options"] = customAttributes
            
            cartArray.append(itemDict)
        }
        params["items"] = cartArray
        
        var request = URLRequest(url: URL(string: "\(env)/api/stripe/create-payment-intent/")!,
                                 timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        do {
            let data = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            request.httpBody = data
        } catch {
            print("error")
        }

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            guard
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any],
                let paymentIntentClientSecret = json["paymentIntent"] as? String
            else {
                completion(nil)
                return
            }
            
            completion(paymentIntentClientSecret)
        }
        task.resume()
    }
}
