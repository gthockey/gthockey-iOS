//
//  NetworkingManager.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 9/30/21.
//  Copyright © 2021 Caleb Rudnicki. All rights reserved.
//

import Foundation

class NetworkingManager {
    
    // MARK: Properties
    
    var env: String {
        #if DEBUG
            return "http://127.0.0.1:8000"
        #elseif TEST
            return "https://test.gthockey.com"
        #else
            return "https://gthockey.com"
        #endif
    }
    
}
