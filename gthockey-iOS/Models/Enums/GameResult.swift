//
//  GameResult.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 6/4/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import Foundation

enum GameResult: String, Codable {
    case Win = "W"
    case Loss = "L"
    case Tie = "T"
    case OvertimeLoss = "OTL"
    case Unknown = "U"
    case NotReported = "?" // When a game's date is in the past but there is no result

    var description: String {
        switch self {
        case .Win: return "Win"
        case .Loss: return "Loss"
        case .Tie: return "Tie"
        case .OvertimeLoss: return "OT Loss"
        case .Unknown: return "Unknown"
        case .NotReported: return "Not Reported"
        }
    }
}
