//
//  ApparelCustomItem.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 11/8/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//

import Foundation

class ApparelCustomItem {
    let id: Int
    let displayName: String
    let helpText: String
    let isRequired: Bool
    let extraCost: Double
    let correspondingApparelID: Int
    var value: String?
    
    init(id: Int, displayName: String, helpText: String, isRequired: Bool, extraCost: Double, correspondingApparelID: Int, value: String? = nil) {
        self.id = id
        self.displayName = displayName
        self.helpText = helpText
        self.isRequired = isRequired
        self.extraCost = extraCost
        self.correspondingApparelID = correspondingApparelID
        self.value = value
    }
}
