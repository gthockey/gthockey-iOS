//
//  ApparelRestrictedItem.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 11/8/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//

import Foundation

class ApparelRestrictedItem {
    let id: Int
    let displayName: String
    let helpText: String
    let optionsList: [String]
    let correspondingApparelID: Int
    var value: String?
    
    init(id: Int, displayName: String, helpText: String, optionsList: [String], correspondingApparelID: Int, value: String? = nil) {
        self.id = id
        self.displayName = displayName
        self.helpText = helpText
        self.optionsList = optionsList
        self.correspondingApparelID = correspondingApparelID
        self.value = value
    }
}
