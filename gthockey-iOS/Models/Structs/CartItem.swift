//
//  CartItem.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 11/15/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//

import Foundation

struct CartItem {
    
    var id: Int
    var name: String
    var imageURL: URL
    var price: Double
    var restrictedAttributes: [CartAttribute]
    var customAttributes: [CartAttribute]
    
    // MARK: Getters
    
    func getPrice() -> Double {
        return price * 100.0
    }

    func getPriceString() -> String {
        return "$" + String(format: "%.2f", price)
    }
    
}

struct CartAttribute {
    var id: Int
    var key: String
    var value: String
    
    func asArray() -> [String : Any] {
        return ["id": id, "key": key, "value": value]
    }
}
