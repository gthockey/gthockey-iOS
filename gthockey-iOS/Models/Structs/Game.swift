//
//  Game.swift
//  gthockey-iOS
//
//  Created by Dylan Mace on 9/27/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//

import Foundation

struct Game: Codable, Hashable {
    
    // MARK: Init
    
    init(id: Int, date: String, time: String, opponent: Team, venue: Venue, rink: Rink,
         season: Season, gtScore: Int?, opponentScore: Int?, shortResult: GameResult) {
        self.id = id
        self.timestamp = (date + " " + time).testDate
        self.date = date
        self.time = time
        self.opponent = opponent
        self.venue = venue
        self.rink = rink
        self.season = season
        self.gtScore = gtScore
        self.opponentScore = opponentScore
        self.shortResult = shortResult
    }
    
    let id: Int
    var timestamp: Date? = nil
    let date: String?
    let time: String?
    let opponent: Team
    let venue: Venue
    let rink: Rink
    let season: Season
    let gtScore: Int?
    let opponentScore: Int?
    let shortResult: GameResult
    
    private enum CodingKeys: String, CodingKey {
        case id
        case date
        case time
        case opponent
        case venue
        case rink = "location"
        case season
        case gtScore = "score_gt_final"
        case opponentScore = "score_opp_final"
        case shortResult = "short_result"
    }
    
    // MARK: Static Functions
    
    static func == (lhs: Game, rhs: Game) -> Bool {
        return lhs.id == rhs.id
    }
}

extension String {
    var testDate: Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.date(from: self)!
    }
}
