//
//  Player.swift
//  gthockey-iOS
//
//  Created by Dylan Mace on 9/27/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//

import Foundation

struct Player {
    let id: Int
    let firstName: String
    let lastName: String
    let position: Position
    let number: Int
    let hometown: String
    let school: String
    let imageURL: URL
    let headshotURL: URL
    let bio: String
}
