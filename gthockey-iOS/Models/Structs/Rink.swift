//
//  Rink.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 10/14/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//

import Foundation

struct Rink: Codable, Hashable {
    let id: Int
    let name: String

    private enum CodingKeys: String, CodingKey {
        case id
        case name = "rink_name"
    }
}
