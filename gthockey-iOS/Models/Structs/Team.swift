//
//  Team.swift
//  gthockey-iOS
//
//  Created by Maksim Tochilkin on 10/8/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//

import Foundation

struct Team: Codable, Hashable {
    let id: Int
    let schoolName: String
    let mascotName: String
    let webURL: URL?
    let logoImageURL: URL
    let backgroundImageURL: URL?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case schoolName = "school_name"
        case mascotName = "mascot_name"
        case webURL = "web_url"
        case logoImageURL = "logo"
        case backgroundImageURL = "background"
    }
}
