//
//  ScheduleCell.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 10/16/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import SwiftUI

struct ScheduleCell: View {
    
    // MARK: Variables
    
    let game: Game
    
    // MARK: Body
    
    var body: some View {
        HStack {
            VStack(spacing: .small) {
                if game.venue == .Home {
                    OpponentScheduleCellComponent(game: game)
                    GTScheduleCellComponent(game: game)
                } else {
                    GTScheduleCellComponent(game: game)
                    OpponentScheduleCellComponent(game: game)
                }
            }
            Spacer()
            Divider()
            AdditionalInfoScheduleCellComponent(game: game)
        }
        .padding([.top, .bottom], .small)
    }
}

struct ScheduleCell_Previews: PreviewProvider {
    static var previews: some View {
        let gthTestData = GTHTestData()
        Group {
            List {
                ScheduleCell(game: gthTestData.gameUpcomingHome)
                ScheduleCell(game: gthTestData.gameCompletedAwayWin)
            }
            .environment(\.colorScheme, .light)
            
            List {
                ScheduleCell(game: gthTestData.gameUpcomingHome)
                ScheduleCell(game: gthTestData.gameCompletedAwayWin)
            }
            .environment(\.colorScheme, .dark)
        }
    }
}
