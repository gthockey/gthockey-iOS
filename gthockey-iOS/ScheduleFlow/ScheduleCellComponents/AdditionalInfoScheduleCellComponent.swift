//
//  AdditionalInfoScheduleCellComponent.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 10/16/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import SwiftUI

struct AdditionalInfoScheduleCellComponent: View {
    
    // MARK: Variables
    
    let game: Game
    
    // MARK: Body
    
    var body: some View {
        VStack(alignment: .center) {
            if game.shortResult == .Win {
                Text(game.shortResult.description)
                    .font(.semiBoldMedium)
                    .foregroundColor(.green)
                Spacer()
                Text(game.timestamp!.mediumDateOnly)
                    .font(.regularSmall)
            } else if game.shortResult == .Loss || game.shortResult == .OvertimeLoss {
                Text(game.shortResult.description)
                    .font(.semiBoldMedium)
                    .foregroundColor(.red)
                Spacer()
                Text(game.timestamp!.mediumDateOnly)
                    .font(.regularSmall)
            } else if game.shortResult == .Tie {
                Text(game.shortResult.description)
                    .font(.semiBoldMedium)
                Spacer()
                Text(game.timestamp!.mediumDateOnly)
                    .font(.regularSmall)
            } else {
                Text(game.timestamp!.mediumDateOnly)
                    .font(.regularSmall)
                Text(game.timestamp!.shortTimeOnly)
                    .font(.regularSmall)
            }
            Text(game.rink.name)
                .font(.regularSmall)
                .multilineTextAlignment(.center)
                .lineLimit(2)
                .truncationMode(.tail)
            
        }
        .frame(maxWidth: 100)
    }
}

struct AdditionalInfoScheduleCellComponent_Previews: PreviewProvider {
    static var previews: some View {
        let gthTestData = GTHTestData()
        Group {
            AdditionalInfoScheduleCellComponent(game: gthTestData.gameUpcomingHome)
                .previewLayout(.fixed(width: 100, height: 85))
            AdditionalInfoScheduleCellComponent(game: gthTestData.gameCompletedHomeWin)
                .previewLayout(.fixed(width: 100, height: 85))
            AdditionalInfoScheduleCellComponent(game: gthTestData.gameCompletedAwayLoss)
                .previewLayout(.fixed(width: 100, height: 85))
        }
    }
}
