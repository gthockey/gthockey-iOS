//
//  GTScheduleCellComponent.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 10/16/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import SwiftUI

struct GTScheduleCellComponent: View {
    
    // MARK: Variables
    
    let game: Game
    
    // MARK: Body
    
    var body: some View {
        HStack {
            Image("BuzzOnlyLogo")
                .resizable()
                .scaledToFit()
                .frame(width: 25, height: 25, alignment: .center)
            Text("Georgia Tech")
                .font(.regularLarge)
            Spacer()
            Text(game.shortResult == .Unknown ? "" : "\(game.gtScore ?? 0)")
                .font(.regularLarge)
        }
    }
}

struct GTScheduleCellComponent_Previews: PreviewProvider {
    static var previews: some View {
        GTScheduleCellComponent(game: GTHTestData().gameUpcomingHome)
            .previewLayout(.fixed(width: 300, height: 40))
    }
}
