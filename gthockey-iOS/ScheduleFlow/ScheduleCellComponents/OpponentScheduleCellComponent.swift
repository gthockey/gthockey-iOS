//
//  OpponentScheduleCellComponent.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 10/16/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct OpponentScheduleCellComponent: View {
    
    // MARK: Variables
    
    let game: Game
    
    // MARK: Body
    
    var body: some View {
        HStack {
            WebImage(url: game.opponent.logoImageURL)
                .resizable()
                .indicator(.activity)
                .transition(.fade)
                .scaledToFit()
                .frame(width: 25, height: 25, alignment: .center)
            Text(game.opponent.schoolName)
                .font(.regularLarge)
            Spacer()
            Text(game.shortResult == .Unknown ? "" : "\(game.opponentScore ?? 0)")
                .font(.regularLarge)
        }
    }
}

struct OpponentScheduleCellComponent_Previews: PreviewProvider {
    static var previews: some View {
        OpponentScheduleCellComponent(game: GTHTestData().gameUpcomingHome)
            .previewLayout(.fixed(width: 300, height: 40))
    }
}
