//
//  ScheduleHeader.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 10/16/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import SwiftUI

struct ScheduleHeader: View {
    
    // MARK: Variables
    
    var isUpcomingGameHeader: Bool
    var seasonRecord: String?
    
    // MARK: Body
    
    var body: some View {
        HStack {
            Text(isUpcomingGameHeader ? "Upcoming" : "Completed")
                .font(.regularLarger)
            if !isUpcomingGameHeader {
                Spacer()
                Text(seasonRecord ?? "0-0-0-0")
                    .font(.regularLarge)
            }
        }
        .foregroundColor(Color(UIColor.scheduleHeaderTitleColor!))
        .padding(.top)
    }
}

struct ScheduleHeader_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ScheduleHeader(isUpcomingGameHeader: true)
                .previewLayout(.fixed(width: 300, height: 60))
            ScheduleHeader(isUpcomingGameHeader: false, seasonRecord: "10-2-1-0")
                .previewLayout(.fixed(width: 300, height: 60))
        }
    }
}
