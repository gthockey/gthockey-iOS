//
//  ScheduleList.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 10/14/20.
//  Copyright © 2020 Caleb Rudnicki. All rights reserved.
//

import SwiftUI

struct ScheduleList: View {
    
    // MARK: State Variables
    
    @State private var seasonArray: [Season] = []
    @State private var completedGameArray: [Game] = []
    @State private var upcomingGameArray: [Game] = []
    @State private var seasonRecord: String = "0-0-0-0"
    
    // MARK: Private Variables
    
    private let contentManager = ContentManager()
    
    // MARK: Public Variables
    
    public var seasonID: Int?
    public var seasonString: String?
    
    // MARK: Body
    
    var body: some View {
        List {
            if completedGameArray.count > 0 {
                Section(header:
                            ScheduleHeader(isUpcomingGameHeader: false, seasonRecord: seasonRecord)
                ) {
                    ForEach(completedGameArray, id:\.self) { game in
                        ScheduleCell(game: game)
                    }
                }
                .listRowBackground(Color(UIColor.gthBackgroundColor!))
            }
            
            if upcomingGameArray.count > 0 {
                Section(header:
                            ScheduleHeader(isUpcomingGameHeader: true)
                ) {
                    ForEach(upcomingGameArray, id:\.self) { game in
                        ScheduleCell(game: game)
                    }
                }
                .listRowBackground(Color(UIColor.gthBackgroundColor!))
            }
        }
        .onAppear {
            self.fetchSchedule()
            UITableView.appearance().backgroundColor = UIColor.gthBackgroundColor
        }
        .navigationBarTitle(seasonString ?? "Schedule")
    }
    
    // MARK: Private Functions
    
    private func fetchSchedule() {
        contentManager.getSchedule(with: seasonID) { response in
            self.completedGameArray = []
            self.upcomingGameArray = []
            
            var wins = 0
            var losses = 0
            var otLosses = 0
            var ties = 0
            
            for game in response {
                if game.shortResult != .Unknown {
                    self.completedGameArray.append(game)
                    switch game.shortResult {
                    case .Win:
                        wins += 1
                    case .Loss:
                        losses += 1
                    case .Tie:
                        ties += 1
                    case .OvertimeLoss:
                        otLosses += 1
                    default: break
                    }
                } else {
                    self.upcomingGameArray.append(game)
                }
            }
            
            self.seasonRecord = "\(wins)-\(losses)-\(otLosses)-\(ties)"
            
//            DispatchQueue.main.async {
//                if self.completedGameArray.count != 0 && self.upcomingGameArray.count != 0 {
//                    // TODO: Scroll to first upcoming game
//                }
//            }
        }
    }
}

struct ScheduleList_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            NavigationView {
                ScheduleList()
                    .environment(\.colorScheme, .light)
                .navigationBarTitle(Text("Schedule"))
            }
            
            NavigationView {
                ScheduleList()
                    .environment(\.colorScheme, .dark)
                .navigationBarTitle(Text("Schedule"))
            }
        }
    }
}
