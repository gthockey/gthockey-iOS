//
//  ShopCustomOptionsView.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 11/8/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//

import UIKit

protocol ShopCustomOptionsViewDelegate {
    func didEnter(option: String, for category: String)
}

class ShopCustomOptionsView: UIView, UITextFieldDelegate {

    // MARK: Properties
    
    public var delegate: ShopCustomOptionsViewDelegate!

    private let displayLabel: UILabel = {
        let displayLabel = UILabel()
        displayLabel.font = UIFont.Oswald.regular.font(size: 20.0)
        displayLabel.adjustsFontSizeToFitWidth = true
        displayLabel.numberOfLines = 1
        displayLabel.textAlignment = .left
        displayLabel.translatesAutoresizingMaskIntoConstraints = false
        return displayLabel
    }()

    private let optionsTextField = ShopOptionTextField()

    // MARK: Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        translatesAutoresizingMaskIntoConstraints = false

        optionsTextField.delegate = self

        addSubviews([displayLabel, optionsTextField])

        updateConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func updateConstraints() {
        super.updateConstraints()

        NSLayoutConstraint.activate([
            displayLabel.topAnchor.constraint(equalTo: topAnchor),
            displayLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            displayLabel.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])

        NSLayoutConstraint.activate([
            optionsTextField.topAnchor.constraint(equalTo: displayLabel.bottomAnchor, constant: 8.0),
            optionsTextField.leadingAnchor.constraint(equalTo: leadingAnchor),
            optionsTextField.trailingAnchor.constraint(equalTo: trailingAnchor),
            optionsTextField.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

    func textFieldDidChangeSelection(_ textField: UITextField) {
        delegate?.didEnter(option: textField.text ?? "", for: displayLabel.text ?? "")
    }

    // MARK: Setter

    public func set(with customItem: ApparelCustomItem) {
        displayLabel.text = customItem.displayName
        optionsTextField.placeholder = customItem.helpText

        customizeTextField(for: customItem.displayName)
    }

    // MARK: Private Functions

    private func customizeTextField(for category: String) {
        if category.lowercased().contains("number") {
            optionsTextField.keyboardType = .numberPad
        } else if category.lowercased().contains("name") {
            optionsTextField.autocapitalizationType = .allCharacters
        }
    }

}
