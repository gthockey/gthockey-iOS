//
//  ShopDetailViewController.swift
//  gthockey-iOS
//
//  Created by Caleb Rudnicki on 11/7/19.
//  Copyright © 2019 Caleb Rudnicki. All rights reserved.
//

import UIKit

class ShopDetailViewController: GTHDetailViewController {

    // MARK: Properties
    
    private var apparelItem: Apparel?
    private var restrictedOptions: [ApparelRestrictedItem]?
    private var customOptions: [ApparelCustomItem]?
    private var inStock: Bool?
    
    private let restrictedOptionsStackView: UIStackView = {
        let restrictedOptionsStackView = UIStackView()
        restrictedOptionsStackView.axis = .vertical
        restrictedOptionsStackView.distribution = .equalCentering
        restrictedOptionsStackView.spacing = 8.0
        restrictedOptionsStackView.translatesAutoresizingMaskIntoConstraints = false
        return restrictedOptionsStackView
    }()

    private let customOptionsStackView: UIStackView = {
        let customOptionsStackView = UIStackView()
        customOptionsStackView.axis = .vertical
        customOptionsStackView.distribution = .equalCentering
        customOptionsStackView.spacing = 8.0
        customOptionsStackView.translatesAutoresizingMaskIntoConstraints = false
        return customOptionsStackView
    }()
    
    private let restrictedOptionsView = ShopRestrictedOptionsView()
    private let customOptionsView = ShopCustomOptionsView()
    
    private let titleStackView: UIStackView = {
        let titleStackView = UIStackView()
        titleStackView.axis = .horizontal
        titleStackView.distribution = .equalCentering
        titleStackView.alignment = .bottom
        titleStackView.spacing = 4.0
        titleStackView.translatesAutoresizingMaskIntoConstraints = false
        return titleStackView
    }()
    
    private let descriptionLabel = HTMLTextView(frame: .zero)
    
    private let addToCartButton = PillButton(title: "Add to Cart",
                                             backgroundColor: UIColor.shopDetailCTAColor!,
                                             borderColor: UIColor.shopDetailCTAColor!,
                                             isEnabled: false)
    
    // MARK: Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
    
        secondaryLabel.numberOfLines = 1
        secondaryLabel.font = UIFont.Oswald.regular.font(size: 24.0)
        secondaryLabel.textColor = UIColor.shopDetailPriceColor
        secondaryLabel.translatesAutoresizingMaskIntoConstraints = false
    
        primaryLabel.numberOfLines = 0
        primaryLabel.font = UIFont.Oswald.regular.font(size: 36.0)
        primaryLabel.textColor = UIColor.shopDetailTitleColor
        primaryLabel.translatesAutoresizingMaskIntoConstraints = false
        
        closeButton.setImage(UIImage(systemName: "xmark.circle.fill",
                                     withConfiguration: UIImage.SymbolConfiguration(pointSize: 36.0)),
                             for: .normal)
        closeButton.tintColor = .label
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        
        addToCartButton.addTarget(self, action: #selector(addToCartButtonTapped), for: .touchUpInside)
        
        titleStackView.addArrangedSubviews([primaryLabel, secondaryLabel])
        
        view.addSubview(scrollView)
        scrollView.addSubviews([imageView, titleStackView, descriptionLabel, restrictedOptionsStackView, customOptionsStackView, addToCartButton, closeButton])
        
        updateViewConstraints()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageView.heightAnchor.constraint(equalTo: view.widthAnchor)
        ])
        
        NSLayoutConstraint.activate([
            titleStackView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 16.0),
            titleStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12.0),
            titleStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -12.0)
        ])
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: titleStackView.bottomAnchor, constant: 12.0),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12.0),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -12.0)
        ])
        
        NSLayoutConstraint.activate([
            restrictedOptionsStackView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 32.0),
            restrictedOptionsStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12.0),
            restrictedOptionsStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -12.0),
        ])
        
        NSLayoutConstraint.activate([
            customOptionsStackView.topAnchor.constraint(equalTo: restrictedOptionsStackView.bottomAnchor, constant: 24.0),
            customOptionsStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12.0),
            customOptionsStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -12.0),
        ])
        
        NSLayoutConstraint.activate([
            addToCartButton.topAnchor.constraint(equalTo: customOptionsStackView.bottomAnchor, constant: 24.0),
            addToCartButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12.0),
            addToCartButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -12.0),
            addToCartButton.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -48.0)
        ])

        NSLayoutConstraint.activate([
            closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 12.0),
            closeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24.0)
        ])
    }
    
    // MARK: Setter

    public func set(with apparel: Apparel, _ restrictedOptions: [ApparelRestrictedItem], _ customOptions: [ApparelCustomItem], _ inStock: Bool) {
        descriptionLabel.setText(with: apparel.description)
        
        if inStock {
            self.restrictedOptions = restrictedOptions
            for restrictedOption in restrictedOptions {
                let shopRestrictedOptionsView = ShopRestrictedOptionsView()
                shopRestrictedOptionsView.set(with: restrictedOption)
                shopRestrictedOptionsView.delegate = self
                restrictedOptionsStackView.addArrangedSubview(shopRestrictedOptionsView)
            }
            
            self.customOptions = customOptions
            for customOption in customOptions {
                let shopCustomOptionsView = ShopCustomOptionsView()
                shopCustomOptionsView.set(with: customOption)
                shopCustomOptionsView.delegate = self
                restrictedOptionsStackView.addArrangedSubview(shopCustomOptionsView)
            }
        } else {
            addToCartButton.setTitle("Out of stock", for: .normal)
        }
        
        apparelItem = apparel
    }
    
    // MARK: Action

    @objc private func addToCartButtonTapped() {
        addToCartButton.isLoading = true
        var price = apparelItem?.price
        var itemDict: [String: Any] = ["id": (apparelItem?.id)!,
                                            "name": (apparelItem?.name)!,
                                            "imageURL": (apparelItem?.imageURL)?.description]
        var restrictedAttributes: [[String : Any]] = []
        var customAttributes: [[String : Any]] = []
        
        guard let restrictedOptions = restrictedOptions else { return }
        for restrictedOption in restrictedOptions {
            if let restrictedOptionValue = restrictedOption.value, restrictedOptionValue != "" {
                let cartAttr = CartAttribute(id: restrictedOption.id,
                                             key: restrictedOption.displayName,
                                             value: restrictedOptionValue)
                restrictedAttributes.append(cartAttr.asArray())
            }
        }
        
        guard let customOptions = customOptions else { return }
        for customOption in customOptions {
            if let customOptionValue = customOption.value, customOptionValue != "" {
                price = (price ?? 0.0) + customOption.extraCost
                let cartAttr = CartAttribute(id: customOption.id,
                                             key: customOption.displayName,
                                             value: customOptionValue)
                customAttributes.append(cartAttr.asArray())
            }
        }
        
        itemDict["price"] = price
        itemDict["restrictedAttributes"] = restrictedAttributes
        itemDict["customAttributes"] = customAttributes
        
        if var cart = UserDefaults.standard.value(forKey: "cart") as? [[String : Any]], !cart.isEmpty {
            // An item already existen in the cart
            cart.append(itemDict)
            UserDefaults.standard.set(cart, forKey: "cart")
        } else {
            // This is the first item in the cart
            UserDefaults.standard.set([itemDict], forKey: "cart")
        }
        
        addToCartButton.isLoading = false
        let alertController = UIAlertController(title: "\((apparelItem?.name)!) added to cart",
                                                message: "See your full cart in the top right corner of the app",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        }))
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: Helper Functions
    
    private func shouldEnableAddToCartButton() -> Bool {
        guard let restrictedOptions = restrictedOptions else { return false }
        for restrictedOption in restrictedOptions {
            if restrictedOption.value == nil || restrictedOption.value == "" {
                return false
            }
        }
        
        guard let customOptions = customOptions else { return false }
        for customOption in customOptions {
            if customOption.isRequired && (customOption.value == nil || customOption.value == "") {
                return false
            }
        }
        
        return true
    }
    
}

extension ShopDetailViewController: ShopRestrictedOptionsViewDelegate {

    func didSelect(option: String, for category: String) {
        guard let restrictedOptions = restrictedOptions else { return }

        for restrictedOption in restrictedOptions {
            if restrictedOption.displayName == category {
                restrictedOption.value = option
            }
        }

        addToCartButton.isEnabled = shouldEnableAddToCartButton()
    }

}

extension ShopDetailViewController: ShopCustomOptionsViewDelegate {

    func didEnter(option: String, for category: String) {
        guard let customOptions = customOptions else { return }

        for customOption in customOptions {
            if customOption.displayName == category {
                customOption.value = option
            }
        }

        addToCartButton.isEnabled = shouldEnableAddToCartButton()
    }

}
